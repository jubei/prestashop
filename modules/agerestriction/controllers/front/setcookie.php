<?php
class AgeRestrictionSetCookieModuleFrontController extends ModuleFrontController
{
  public function initContent()
  {
    parent::initContent();

    if (Tools::getValue('age') && Validate::isInt(Tools::getValue('age'))) {
      $this->context->cookie->agerestriction_cookie = Tools::getValue('age');
      $this->context->smarty->assign(['success'=>true]);
    } else {
      $this->context->smarty->assign(['success'=>false]);
      $this->context->smarty->assign(['error'=>true, 'error_message'=>'Age not valid.']);

    }

     $this->setTemplate('module:agerestriction/views/templates/front/cookie.tpl');

     if ($this->ajax) {
      $this->display();
     }
  }

}