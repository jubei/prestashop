<script type="text/javascript">
  window.addEventListener('load', function(){
      var age_cookie = '{$cookie}';
      if (!age_cookie) {
        $( "#agerestriction-dialog" ).dialog({
          dialogClass: "no-close",
          modal: true,
          buttons: [
            {
              text: "{l s='Yes' mod='agerestriction'}",
              click: function() {
                var _this = this;;
                var query = $.ajax({
                  type: 'POST',
                  url: '{$ajaxURL nofilter}',
                  data: 'age={$age}',
                  //dataType: 'json',
                  success: function(json) {
                     $( _this ).dialog( "close" );
                  }
               });
              }
            },
            {
              text: "{l s='No' mod='agerestriction'}",
              click: function() {
                   window.location = '{$url nofilter}';
              }
            }
          ]
        });
      }
  });
</script>

<div id="agerestriction-dialog" title="{l s='Warning' mod='agerestriction'}">
  <p>{$message}</p>
</div>