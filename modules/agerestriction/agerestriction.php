<?php   
if (!defined('_PS_VERSION_')) {
  exit;
}

define('GLOBAL_FILTER_ALL', 1);
define('GLOBAL_FILTER_NONE', 0);

class AgeRestriction extends Module {
    private $module_active = false;

    public function __construct() {
        $this->name = 'agerestriction';
        $this->tab = 'front_office_features.administration';
        $this->version = '1.0.0';
        $this->author = 'J B';
  
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
        $this->bootstrap = true;
     
        parent::__construct();
  
        $this->displayName = $this->l('Age Restriction PS');
        $this->description = $this->l('Protect your website from underaged audience.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    private function getCategories() {
        $category = new Category(); //Configuration::get('PS_HOME_CATEGORY')

        return $category->getSimpleCategories($this->context->language->id);
    }

    private function getPages() {
        $cms = new CMSCore();

        //var_dump($cms->getCMSPages($this->context->language->id)); die();

        return $cms->getCMSPages($this->context->language->id);
    }

    private function getProducts() {
        $product = new Product();

        return $product->getProducts($this->context->language->id, 0, 9999, 'name', 'ASC' );
    }

    public function displayForm()
    {
        // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        
        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Age'),
                    'name' => 'AGERESTRICTIONMODULE_AGE',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('URL'),
                    'name' => 'AGERESTRICTIONMODULE_URL',
                    'size' => 160,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Message'),
                    'name' => 'AGERESTRICTIONMODULE_MESSAGE',
                    'size' => 20,
                    'required' => true
                ),
                array(
                    'type'=>'select',
                    'label'=>$this->l('Category Redirect'),
                    'name'=>'AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION', //all none
                    'options'=>array(
                        'query' => Array(
                            ['name'=>$this->l('All'), 'value'=>GLOBAL_FILTER_ALL],
                            ['name'=>$this->l('None'),'value'=>GLOBAL_FILTER_NONE]
                        ),
                        'name' => 'name',
                        'id'=>'value' // ?
                    )
                ),
                array(
                    'type'=>'select',
                    'label'=>$this->l('Except categories'),
                    'multiple'=>true,
                    'name'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES', //all none
                    'id'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES',
                    'options'=>array(
                        'query' => $this->getCategories(),
                        'name' => 'name',
                        'id'=>'id_category'
                    )
                    ),
                    array(
                        'type'=>'select',
                        'label'=>$this->l('Product Redirect'),
                        'name'=>'AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION', //all none
                        'options'=>array(
                            'query' => Array(
                                ['name'=>$this->l('All'), 'value'=>GLOBAL_FILTER_ALL],
                                ['name'=>$this->l('None'),'value'=>GLOBAL_FILTER_NONE]
                            ),
                            'name' => 'name',
                            'id'=>'value' // ?
                        )
                    ),
                array(
                        'type'=>'select',
                        'label'=>$this->l('Except products'),
                        'multiple'=>true,
                        'name'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS', //all none
                        'id'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS',
                        'options'=>array(
                            'query' => $this->getProducts(),
                            'name' => 'name',
                            'id'=>'id_product'
                        )
                        ),
                        array(
                            'type'=>'select',
                            'label'=>$this->l('CMS Pages'),
                            'name'=>'AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION', //all none
                            'options'=>array(
                                'query' => Array(
                                    ['name'=>$this->l('All'), 'value'=>GLOBAL_FILTER_ALL],
                                    ['name'=>$this->l('None'),'value'=>GLOBAL_FILTER_NONE]
                                ),
                                'name' => 'name',
                                'id'=>'value' // ?
                            )
                        ),
                        array(
                            'type'=>'select',
                            'label'=>$this->l('Except CMS Pages'),
                            'multiple'=>true,
                            'name'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES', //all none
                            'id'=>'AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES',
                            'options'=>array(
                                'query' => $this->getPages(),
                                'name' => 'meta_title',
                                'id'=>'id_cms'
                            )
                        )                 
            ),                  
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );
        
    $helper = new HelperForm();
     
    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
     
    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;
     
    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );
     
    // Load current value
    $helper->fields_value['AGERESTRICTIONMODULE_AGE'] = Configuration::get('AGERESTRICTIONMODULE_AGE');
    $helper->fields_value['AGERESTRICTIONMODULE_URL'] = Configuration::get('AGERESTRICTIONMODULE_URL');
    $helper->fields_value['AGERESTRICTIONMODULE_MESSAGE'] = Configuration::get('AGERESTRICTIONMODULE_MESSAGE');
    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION'] = Configuration::get('AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION');
    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES[]'] = unserialize(Configuration::get('AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES'));

    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION'] = Configuration::get('AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION');
    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS[]'] = unserialize(Configuration::get('AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS'));

    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION'] = Configuration::get('AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION');
    $helper->fields_value['AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES[]'] = unserialize(Configuration::get('AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES'));

     
    return $helper->generateForm($fields_form);
}

public function getContent()
{
    $output = null;
 
    if (Tools::isSubmit('submit'.$this->name))
    {
        $agerestriction_age = Tools::getValue('AGERESTRICTIONMODULE_AGE');
        $agerestriction_url = strval(Tools::getValue('AGERESTRICTIONMODULE_URL'));
        $agerestriction_message = strval(Tools::getValue('AGERESTRICTIONMODULE_MESSAGE'));

        $agerestriction_filter_category_option = Tools::getValue('AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION');
        $agerestriction_filter_except_categories = Tools::getValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES');

        $agerestriction_filter_product_option = Tools::getValue('AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION');
        $agerestriction_filter_except_products = Tools::getValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS');
   
        $agerestriction_filter_cms_page_option = Tools::getValue('AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION');
        $agerestriction_filter_except_cms_pages = Tools::getValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES');

        if (!$agerestriction_age ||
            !$agerestriction_url ||
            !$agerestriction_message
          || empty($agerestriction_age)
          || empty($agerestriction_url)
          || !Validate::isInt($agerestriction_age)
          || !Validate::isInt($agerestriction_filter_category_option)
          || !Validate::isInt($agerestriction_filter_product_option)
          || !Validate::isInt($agerestriction_filter_cms_page_option)
          || !Validate::isUrl($agerestriction_url)
          || strlen($agerestriction_message) == 0)
            $output .= $this->displayError($this->l('Invalid Configuration value'));
        else
        {
            Configuration::updateValue('AGERESTRICTIONMODULE_AGE', $agerestriction_age);
            Configuration::updateValue('AGERESTRICTIONMODULE_URL', $agerestriction_url);
            Configuration::updateValue('AGERESTRICTIONMODULE_MESSAGE', $agerestriction_message);
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION', $agerestriction_filter_category_option);
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES', serialize($agerestriction_filter_except_categories ? $agerestriction_filter_except_categories : []));
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION', $agerestriction_filter_product_option);
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS', serialize($agerestriction_filter_except_products ? $agerestriction_filter_except_products : []));
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION', $agerestriction_filter_cms_page_option);
            Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES', serialize($agerestriction_filter_except_cms_pages));
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
    }
    return $output.$this->displayForm();
}

public function setMedia()
{
    $this->addJquery();
}

public function redirectCurrent($active_controller) {
    if ($active_controller == 'index') {
        $id = Configuration::get('PS_HOME_CATEGORY');
        $filter = "CATEGORY"; // assuming in Prestashop homepage is always a category...
        $filter_plural = "CATEGORIES";
    }

    if ($active_controller == 'category') {
        $id = Tools::getValue('id_category');
        $filter = "CATEGORY";
        $filter_plural = "CATEGORIES";
    }

    if ($active_controller == 'product') {
        $id = Tools::getValue('id_product');
        $filter = "PRODUCT";
        $filter_plural = "PRODUCTS";
    }

    if ($active_controller == 'cms') {
        $id = Tools::getValue('id_cms');
        $filter = "CMS_PAGE";
        $filter_plural = "CMS_PAGES";
    }

    if (isset($id) && Configuration::get("AGERESTRICTIONMODULE_FILTER_{$filter}_OPTION") == GLOBAL_FILTER_ALL) {
        if (Configuration::get("AGERESTRICTIONMODULE_FILTER_EXCEPT_{$filter_plural}") && in_array($id, unserialize(Configuration::get("AGERESTRICTIONMODULE_FILTER_EXCEPT_{$filter_plural}")))) {
            return false;
        } 

        return true;
    }

    if (isset($id) && Configuration::get("AGERESTRICTIONMODULE_FILTER_{$filter}_OPTION") == GLOBAL_FILTER_NONE) {
        if (Configuration::get("AGERESTRICTIONMODULE_FILTER_EXCEPT_{$filter_plural}") && in_array($id, unserialize(Configuration::get("AGERESTRICTIONMODULE_FILTER_EXCEPT_{$filter_plural}")))) {
            return true;
        }

        return false;
    }

    return false;
}


public function hookDisplayHeader() {
    if ($this->redirectCurrent(Dispatcher::getInstance()->getController())) {
        $this->context->controller->addCSS($this->_path.'css/agerestriction.css', 'all');
        $this->module_active = true;
    }
}
   
public function hookDisplayFooter()
{ 
    if (!$this->module_active) return;

  $this->context->smarty->assign(
    array(
        'age' => Configuration::get('AGERESTRICTIONMODULE_AGE'),
        'url' => Configuration::get('AGERESTRICTIONMODULE_URL'),
        'message' => sprintf(Configuration::get('AGERESTRICTIONMODULE_MESSAGE'), Configuration::get('AGERESTRICTIONMODULE_AGE')),
        'cookie'=> $this->context->cookie->agerestriction_cookie,
        'ajaxURL' => $this->context->link->getModuleLink('agerestriction', 'setcookie')
    )
);

  return $this->display(__FILE__, 'agerestriction.tpl');
}   

    public function install() {
        if (!parent::install() ||
        !$this->registerHook('footer') || 
        !$this->registerHook('header') || 
        !Configuration::updateValue('AGERESTRICTIONMODULE_AGE', '21') ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_URL', 'http://www.example.com') ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_MESSAGE', 'You must be %s to enter this site. Are you?') ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION', GLOBAL_FILTER_NONE) ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES', serialize([])) ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION', GLOBAL_FILTER_NONE) ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS', serialize([])) ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION', GLOBAL_FILTER_ALL) ||
        !Configuration::updateValue('AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES', serialize([]))

        ) {
            return false;
        }

        return true;
    }

    public function uninstall() {
        if (!parent::uninstall() ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_AGE') || 
        !Configuration::deleteByName('AGERESTRICTIONMODULE_URL') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_MESSAGE') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_CATEGORY_OPTION') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_EXCEPT_CATEGORIES') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_PRODUCT_OPTION') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_EXCEPT_PRODUCTS') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_CMS_PAGE_OPTION') ||
        !Configuration::deleteByName('AGERESTRICTIONMODULE_FILTER_EXCEPT_CMS_PAGES')
        ) {
            return false;
        }
        
        return true;
    }
}